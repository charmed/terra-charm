# Cloudfront Distribution Record
resource "aws_route53_record" "client" {
  zone_id = var.hosted_zone_id
  name    = "${var.subdomain}${var.domain}"
  type    = "A"

  alias {
    evaluate_target_health = true
    name                   = aws_cloudfront_distribution.default.domain_name
    zone_id                = aws_cloudfront_distribution.default.hosted_zone_id
  }
}

# API Gateway Record
resource "aws_api_gateway_domain_name" "default" {
  regional_certificate_arn = aws_acm_certificate_validation.api.certificate_arn
  domain_name              = "api.${var.subdomain}${var.domain}"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_route53_record" "api" {
  zone_id = var.hosted_zone_id
  name    = aws_api_gateway_domain_name.default.domain_name
  type    = "A"

  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.default.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.default.regional_zone_id
  }
}
