
resource "aws_acm_certificate" "client" {
  domain_name       = "${var.subdomain}${var.domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application = "Charmed"
    Environment = var.env
  }
}

resource "aws_acm_certificate" "api" {
  domain_name       = "api.${var.subdomain}${var.domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
  
  tags = {
    Application = "Charmed"
    Environment = var.env
  }
}
resource "aws_route53_record" "client_validation" {
  zone_id = var.hosted_zone_id
  for_each = {
    for dvo in aws_acm_certificate.client.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      type   = dvo.resource_record_type
      record = dvo.resource_record_value
    }
  }
  name            = each.value.name
  type            = each.value.type
  records         = [each.value.record]
  ttl     = 60
}

resource "aws_route53_record" "api_validation" {
  zone_id = var.hosted_zone_id
  for_each = {
    for dvo in aws_acm_certificate.api.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      type   = dvo.resource_record_type
      record = dvo.resource_record_value
    }
  }
  name            = each.value.name
  type            = each.value.type
  records         = [each.value.record]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "client" {
  certificate_arn         = aws_acm_certificate.client.arn
  validation_record_fqdns = [for record in aws_route53_record.client_validation : record.fqdn]

}

resource "aws_acm_certificate_validation" "api" {
  certificate_arn         = aws_acm_certificate.api.arn
  validation_record_fqdns = [for record in aws_route53_record.api_validation : record.fqdn]
}
