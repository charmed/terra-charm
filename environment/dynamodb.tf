resource "aws_dynamodb_table" "default" {
  name           = var.env
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "userId"
  range_key      = "classId"

  attribute {
    name = "userId"
    type = "S"
  }

  attribute {
    name = "classId"
    type = "S"
  }

  tags = {
    Application = "Charmed"
    Environment = var.env
  }
}
