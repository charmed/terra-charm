resource "aws_cloudfront_origin_access_identity" "default" {}
resource "aws_cloudfront_distribution" "default" {
  origin {
    domain_name = aws_s3_bucket.client.bucket_domain_name
    origin_id = "origin-bucket-${aws_s3_bucket.client.id}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path
    }
  }

  enabled = true
  is_ipv6_enabled = true
  default_root_object = "index.html"
  price_class = "PriceClass_100"

  aliases = ["${var.subdomain}${var.domain}"]

  default_cache_behavior {
    allowed_methods = ["HEAD", "GET"]
    cached_methods = ["HEAD", "GET"]
    target_origin_id = "origin-bucket-${aws_s3_bucket.client.id}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    compress = true
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
  }


  ordered_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    compress = false
    default_ttl = 86400
    max_ttl = 0
    min_ttl = 0
    path_pattern = "service-worker.js"
    smooth_streaming = false
    target_origin_id = "origin-bucket-${aws_s3_bucket.client.id}"
    trusted_signers = []
    viewer_protocol_policy = "allow-all"
    forwarded_values {
      headers = []
      query_string = false
      query_string_cache_keys = []
      cookies {
        forward = "none"
        whitelisted_names = []
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  custom_error_response {
    error_code = 403
    response_code = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code = 404
    response_code = 200
    response_page_path = "/index.html"
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.client.arn
    ssl_support_method = "sni-only"
  }

  tags = {
    Application = "Charmed"
    Environment = var.env
  }
}
