# Point the API Gateway Domain name at its latest stage
resource "aws_api_gateway_base_path_mapping" "default" {
  api_id      = var.api_id
  stage_name  = var.api_stage_name
  domain_name = aws_api_gateway_domain_name.default.domain_name
}

# Give this API's Lambda executor access to its dynamoDB table
resource "aws_iam_policy" "full_access_dynamodb" {
  policy = data.aws_iam_policy_document.full_access_dynamodb.json
}

data "aws_iam_policy_document" "full_access_dynamodb" {
  statement {
    actions = [
      "dynamodb:*",
    ]

    resources = [
      aws_dynamodb_table.default.arn
    ]
  }
}

resource "aws_iam_role_policy_attachment" "lambda__dynamodb_access" {
  role       = var.lambda_role_name
  policy_arn = aws_iam_policy.full_access_dynamodb.arn
}