# Bucket to store client code

resource "aws_s3_bucket" "client" {
  bucket = "client.${var.env}.${var.domain}.${var.iteration}"
  acl    = "private"
  policy = data.aws_iam_policy_document.client.json
  tags = {
    Application = "Charmed"
    Environment = var.env
  }
}

data "aws_iam_policy_document" "client" {
  statement {
    sid = "OnlyCloudfrontReadAccess"
    actions = ["s3:GetObject"]
    principals {
      type        = "CanonicalUser"
      identifiers = [aws_cloudfront_origin_access_identity.default.s3_canonical_user_id]
    }
    resources = ["arn:aws:s3:::client.${var.env}.${var.domain}.${var.iteration}/*"]
  }
}
