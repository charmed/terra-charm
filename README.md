
Before running on a new charmed account make sure to set up the API Gateway/Lambda resources with lambda-charm.

After creating the hosted zones you'll need to kill the process and "glue" the nameservers for the
registered domain to the main hosted zone because terraform currently can't do that.
