# Must update bucket name manually each iteration...
terraform {
  required_version = "1.3.4"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.39.0"
    }
  }
  backend "s3" {
    bucket = "meta.charmed.cloud.0"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

# This bucket holds the .tfstate file that contains the most recent state of everything terraform has managed for this environment.
provider "aws" {
  region = "us-east-1"
}

# Increment every migration to ensure globally-unique bucket names
variable "iteration" {
  default = "0"
}

# Domain name
variable "domain" {
  default = "charmed.cloud"
}

# ClaudiaJS defaults this to "latest"
variable "api_stage_name" {
  default = "latest"
}

# ClaudiaJS defaults this to "latest"
variable "aws_account_id" {
  default = "705904441410"
}

# Environments
resource "aws_route53_zone" "default" {
  name = var.domain
  tags = {
    Application = "Charmed"
  }
}

module "stage" {
  source           = "./environment"
  env              = "stage"
  subdomain        = "stage."
  iteration        = var.iteration
  api_stage_name   = var.api_stage_name
  domain           = var.domain
  hosted_zone_id   = aws_route53_zone.default.zone_id
  api_id           = "x3dkat8bpc"
  lambda_role_name = "Stage-executor"
}

module "production" {
  source           = "./environment"
  env              = "production"
  subdomain        = ""
  iteration        = var.iteration
  api_stage_name   = var.api_stage_name
  domain           = var.domain
  hosted_zone_id   = aws_route53_zone.default.zone_id
  api_id           = "lladyowmng"
  lambda_role_name = "Production-executor"
}
